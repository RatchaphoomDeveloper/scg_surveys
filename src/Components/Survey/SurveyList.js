import React from "react";
import Select from "react-select";
import { useHistory } from "react-router-dom";
import "./SurVeyList.scss";
const SurveyList = () => {
  const history = useHistory();
  const selectName = [
    {
      value: "",
      label: "แบบสอบถาม: ทั้งหมด",
    },
    {
      value: "",
      label: "",
    },
    {
      value: "",
      label: "",
    },
  ];

  const selectTime = [
    {
      value: "",
      label: "ช่วงเวลาทั้งหมด",
    },
    {
      value: "",
      label: "",
    },
    {
      value: "",
      label: "",
    },
  ];

  const contentData = [
    {
      contentLabel: "แบบสำรวจเทศการวันหยุดแรงงาน",
      timeLabel: "วันที่สร้าง : 04 ธ.ค. 63",
      countUser: "50",
    },
    {
      contentLabel: "แบบสำรวจเทศการวันหยุดแรงงาน",
      timeLabel: "วันที่สร้าง : 10 ธ.ค. 63",
      countUser: "50",
    },
    {
      contentLabel: "แบบสำรวจเทศการวันหยุดปีใหม่",
      timeLabel: "วันที่สร้าง : 25 ธ.ค. 63",
      countUser: "50",
    },
  ];

  return (
    <section>
      <div className="title-container-arround">
        <div className="grap">
          <div className="dash-head-content">
            <h2 className="dash-tittle">แบบสอบถามทั้งมด :</h2>
            <div className="border-number">
              <h4>3</h4>
            </div>
          </div>

          <div className="survey-list-filter-content">
            <div className="survey-select">
              <div className="form-select-group">
                <select
                  name="type_com"
                  id="type_com"
                  //   onChange={(e) => setDomain(e.target.value)}
                >
                  <option value="0" selected>
                    --กรุณาเลือกชนิดของผู้ใช้งาน--
                  </option>
                  {selectName &&
                    selectName.map((res, i) => {
                      return <option value={res.value}>{res.label}</option>;
                    })}
                </select>
              </div>
            </div>
            <div className="survey-select">
              <div className="form-select-group">
                <select
                  name="type_com"
                  id="type_com"
                  //   onChange={(e) => setDomain(e.target.value)}
                >
                  <option value="0" selected>
                    --กรุณาเลือกชนิดของผู้ใช้งาน--
                  </option>
                  {selectTime &&
                    selectTime.map((res, i) => {
                      return <option value={res.value}>{res.label}</option>;
                    })}
                </select>
              </div>
            </div>
            <button
              type="button"
              className="md"
              onClick={() => {
                history.push("/survey_list/create");
              }}
            >
              <span className="fas fa-plus"></span>
              <label>สร้างแบบสำรวจ</label>
            </button>
          </div>
        </div>
      </div>
      {contentData &&
        contentData.map((res, i) => {
          return (
            <div className="surver-list-card" key={i}>
              <div>
                <h3>{res.contentLabel}</h3>
                <p>{res.timeLabel}</p>
                <p>
                  {" "}
                  จำนวนผู้สำรวจ :{" "}
                  <span className="border-number-list">
                    {res.countUser}
                  </span>{" "}
                </p>
              </div>
              <div className="collapse">
                <span className="fas fa-chevron-right"></span>
              </div>
            </div>
          );
        })}
    </section>
  );
};

export default SurveyList;
