import React, { useEffect, useState } from "react";
import moment from "moment";
import Helmet from "react-helmet";
import { formatDate, parseDate } from "react-day-picker/moment";
import DayPickerInput from "react-day-picker/DayPickerInput";
import "./SurVeyList.scss";
import { useHistory } from "react-router-dom";

const CreateSurVey = () => {
  const history = useHistory();
  //   const [from, setFrom] = useState(undefined);
  //   const [to, setTo] = useState(undefined);
  const [state, setState] = useState({
    from: undefined,
    to: undefined,
  });
  const showFromMonth = () => {
    if (!state.from) {
      return;
    }
    if (moment(state.to).diff(moment(state.from), "months") < 2) {
      this.to.getDayPicker().showMonth(state.from);
    }
  };
  const handleFromChange = (from) => {
    // Change the from date and focus the "to" input field
    setState(from);
  };

  const handleToChange = (to) => {
    setState({ to }, showFromMonth);
  };
  const { from, to } = state;
  const modifiers = { start: from, end: to };
  const selectTime = [
    {
      value: "",
      label: "โรงงาน 1",
    },
    {
      value: "",
      label: "โรงงาน 2",
    },
    {
      value: "",
      label: "โรงงาน 3",
    },
  ];
  return (
    <div>
      <div className="surver-create-card">
        <h3 id="name-survey">1. ชื้อแบบสำรวจ</h3>
        <div className="survey-form-group">
          <input type="text" />
        </div>
        <h3>2. วันที่สร้างแบบสำรวจ</h3>
        <div className="survey-form-date-group">
          <div className="InputFromTo">
            <DayPickerInput
              value={from}
              placeholder="From"
              format="MM/DD/yyyy"
              formatDate={formatDate}
              parseDate={parseDate}
              dayPickerProps={{
                selectedDays: [from, { from, to }],
                disabledDays: { after: to },
                toMonth: to,
                modifiers,
                numberOfMonths: 2,
                onDayClick: () => state.to.getInput().focus(),
              }}
              onDayChange={handleFromChange}
            />{" "}
            —{" "}
            <span className="InputFromTo-to">
              <DayPickerInput
                ref={(el) => (state.to = el)}
                value={to}
                placeholder="To"
                format="MM/DD/yyyy"
                formatDate={formatDate}
                parseDate={parseDate}
                dayPickerProps={{
                  selectedDays: [from, { from, to }],
                  disabledDays: { before: from },
                  modifiers,
                  month: from,
                  fromMonth: from,
                  numberOfMonths: 2,
                }}
                onDayChange={handleToChange}
              />
            </span>
            <Helmet>
              <style>{`
  .InputFromTo .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
    background-color: #f0f8ff !important;
    color: #4a90e2;
  }
  .InputFromTo .DayPicker-Day {
    border-radius: 0 !important;
  }
  .InputFromTo .DayPicker-Day--start {
    border-top-left-radius: 50% !important;
    border-bottom-left-radius: 50% !important;
  }
  .InputFromTo .DayPicker-Day--end {
    border-top-right-radius: 50% !important;
    border-bottom-right-radius: 50% !important;
  }
  .InputFromTo .DayPickerInput-Overlay {
    width: 550px;
  }
  .InputFromTo-to .DayPickerInput-Overlay {
    margin-left: -198px;
  }
`}</style>
            </Helmet>
          </div>
        </div>
        <h3>2. วันที่สร้างแบบสำรวจ</h3>
        <div className="survey-form-group">
          <select
            name="type_com"
            id="type_com"
            //   onChange={(e) => setDomain(e.target.value)}
          >
            <option value="0" selected>
              --กรุณาเลือกชนิดของผู้ใช้งาน--
            </option>
            {selectTime &&
              selectTime.map((res, i) => {
                return <option value={res.value}>{res.label}</option>;
              })}
          </select>
        </div>
        <div className="form-button-group">
          <button
            type="button"
            className="md"
            onClick={() => {
              history.push("/survey_list/create/surveyEditor");
            }}
          >
            <label>สร้างแบบสำรวจ</label>
          </button>
        </div>
      </div>
    </div>
  );
};

export default CreateSurVey;
