import React, { useState, useEffect } from "react";
import * as Survey from "survey-knockout";
import * as SurveyCreator from "survey-creator";
import "survey-knockout/survey.css";
import "survey-creator/survey-creator.css";

SurveyCreator.StylesManager.applyTheme("darkblue");
var localStorageName = "SaveLoadSurveyCreatorExample";
const SurveyEditor = () => {
  const [question, setQuestion] = useState("");
  useEffect(() => {
    var options = {
      showLogicTab: false,
      isAutoSave: true,
      showTestSurveyTab: true,
      showJSONEditorTab: false,
      showPropertyGrid: true,
      questionTypes: ["text","radiogroup"],
      showDropdownPageSelector: false,
      pageEditMode: "standard",
      hideAdvancedSettings: true,
    };
    var creator = new SurveyCreator.SurveyCreator("creatorElement", options);
    var surveySettingsAction = creator.toolbarItems().filter(function (item) {
      return item.id === "svd-survey-settings";
    })[0];
    creator.saveSurveyFunc = function (saveNo, callback) {
        //save the survey JSON
        //You can store in your database JSON as text: creator.text  or as JSON: creator.JSON
        window.localStorage.setItem(localStorageName, creator.text);
  
        //We assume that we can't get error on saving data in local storage
        //Tells creator that changing (saveNo) saved successfully.
        //Creator will update the status from Saving to saved
        callback(saveNo, true);
      };
  });

  return (
    <div>
      <div className="mb-2">
        <div id="creatorElement" />
      </div>
    </div>
  );
};

export default SurveyEditor;
