import React from "react";
import { Route, Link, Switch } from "react-router-dom";
import Main from "../Main/Main";
import CreateSurVey from "../Survey/CreateSurVey";
import SurveyEditor from "../Survey/SurveyEditor";
import SurveyList from "../Survey/SurveyList";
import "./SideBar.scss";
const SideBar = () => {
  return (
    <div>
      <div className="sidebar">
        <div className="sidebar-header">
          <h2>SCG SURVEYS</h2>

          <span className="fa fa-bars"></span>
        </div>
        <div className="sidebar-menu">
          <div className="">
            <ul>
              <li>
                <Link to={"/"}>
                  <span className="fas fa-home"></span>
                  <span>หน้าหลัก</span>
                </Link>
              </li>
              <li>
                <Link to={"/survey_list"}>
                  <span className="fas fa-pencil-alt"></span>
                  <span>ร่างแบบสอบถาม</span>
                </Link>
              </li>
            </ul>
            <div className="main-side"></div>
          </div>

          <div className="foot-side">
            <p id="menu-p">จัดการข้อมูลในระบบ</p>
            <div className="">
              <ul>
                <li>
                  <label for="btn-1" className="text-menu">
                    <div>
                      <span id="icon-sub" className="fas fa-cog"></span>
                      <span>ผู้ใช้งาน</span>
                    </div>
                    <span id="right" class="fas fa-chevron-right"></span>
                  </label>
                  <input type="checkbox" id="btn-1" />
                  <ul>
                    <li>
                      <Link to={"/"}>
                        <span id="sub-menu">ผุ้ใช้งานระบบ</span>
                      </Link>
                    </li>
                  </ul>
                </li>
                <li>
                  {/* <Link to={"/"}>
                    
                  </Link> */}
                  <label for="btn-2" className="text-menu">
                    <div>
                      <span id="icon-sub" className="fas fa-cog"></span>
                      <span>ตั้งค่าระบบ</span>
                    </div>
                    <span id="right" class="fas fa-chevron-right"></span>
                  </label>
                  <input type="checkbox" id="btn-2"  />
                  <ul>
                    <li>
                      <Link to={"/"}>
                        <span id="sub-menu">แบบสำรวจ</span>
                      </Link>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>

            <div className="main-side"></div>
          </div>
        </div>
      </div>
      <div className="main-content">
        <header>
          <div className="search-wrapper">
            <span className="fas fa-search"></span>
            <input type="search" placeholder="ค้นหา" />
          </div>
          <div className="social-icons">
            <span className="far fa-bell"></span>
            <span className="far fa-comment-alt"></span>
            <span className="fas fa-user-circle"></span>
          </div>
        </header>
        <main>
          <Switch>
            <Route exact path="/" component={Main} />
            <Route exact path="/survey_list" component={SurveyList} />
            <Route exact  path="/survey_list/create" component={CreateSurVey} />
            <Route exact  path="/survey_list/create/surveyEditor" component={SurveyEditor} />
            {/* <Route exact  path="/login_log" component={Log} />
              <Route exact path="/create_template" component={Template} />
              <Route exact path="/create_editor_page" component={Editor} />
              <Route exact path="/consider_user" component={ConsiderUser}/>
              <Route exact path="/user_group" component={UserGroup}/> */}
          </Switch>
        </main>
      </div>
    </div>
  );
};

export default SideBar;
