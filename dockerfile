# FROM node:14 as build

# ARG REACT_APP_SERVICES_HOST=/services/m

# WORKDIR '/app'

# COPY ./package.json /app/package.json/
# COPY ./package-lock.json /app/package-lock.json/
# COPY . /app/

# RUN npm install --slient
# COPY . ./
# RUN npm run build

FROM tiangolo/node-frontend:10 as build
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app/
RUN npm run build


FROM nginx
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY  nginx/nginx.conf /etc/nginx/conf.d

EXPOSE 80
CMD ["nginx","-g","daemon off;"]
